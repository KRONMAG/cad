﻿
namespace DomainModel
{
    public abstract class Vertex
    {
        public abstract string Name { get; }

        public override string ToString() =>
            Name;

        public override int GetHashCode() =>
            Name.GetHashCode();

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj is Vertex)
                return ((Vertex)obj).Name == Name;
            return base.Equals(obj);
        }
    }
}