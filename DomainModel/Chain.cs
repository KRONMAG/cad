﻿using CodeContracts;

namespace DomainModel
{
    public class Chain : Vertex
    {
        public override string Name { get; }

        public Chain(string name)
        {
            Requires.NotNull(name, nameof(name), "Имя цепи не может быть равно null");
            Requires.NotNullOrEmpty(name, nameof(name), "Имя цепи не может быть пустым");

            Name = name;
        }
    }
}