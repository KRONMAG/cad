﻿using CodeContracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DomainModel
{
    public class SchemeReader
    {
        public Dictionary<Chain, HashSet<Element>> Read(string path)
        {
            Requires.NotNull
            (
                path,
                nameof(path),
                "Путь к файлу с описанием схемы не может быть равен null"
            );
            Requires.True
            (
                File.Exists(path),
                "Не удалось найти файл с описанием схемы по указанному пути"
            );

            var lines = File.ReadAllLines(path)
                .Where(line => !string.IsNullOrWhiteSpace(line))
                .Select(line => line.Trim('\t', ' '))
                .ToList();

            if (lines.Any(line => line == "$NETS"))
                return ReadAllegro(lines);
            return ReadCl90(lines);
        }

        private Dictionary<Chain, HashSet<Element>> ReadAllegro(List<string> lines)
        {
            var chains = new Dictionary<Chain, HashSet<Element>>();
            Chain lastReadedChain = null;
            lines.SkipWhile(line => line != "$NETS")
                .Where(line => line != "$NETS" && line != "$END")
                .ToList()
                .ForEach
                (
                    line =>
                    {
                        var items = line.Split
                        (
                            separator: new[] { ';', ' ', '\t', ',' },
                            options: StringSplitOptions.RemoveEmptyEntries
                        );

                        var elements = items
                            .Skip(line.Contains(";") ? 1 : 0)
                            .Select
                            (
                                element => new Element
                                (
                                    new string
                                    (
                                        element.TakeWhile(symbol => symbol != '.').ToArray()
                                    )
                                )
                            ).ToHashSet();

                        if (line.Contains(";"))
                        {
                            lastReadedChain = new Chain(items[0]);
                            chains.Add(lastReadedChain, elements);
                        }
                        else
                        {
                            chains[lastReadedChain].UnionWith(elements);
                        }
                    }
                );

            return chains;
        }

        private Dictionary<Chain, HashSet<Element>> ReadCl90(List<string> lines)
        {
            var chains = new Dictionary<Chain, HashSet<Element>>();
            Chain lastReadedChain = null;
            lines.ForEach
            (
                line =>
                {
                    var items = line.Split
                    (
                        separator: new[] { ' ', '\t', ';', ',' },
                        options: StringSplitOptions.RemoveEmptyEntries
                    );

                    var elements = items
                        .Skip(lastReadedChain == null ? 1 : 0)
                        .Select
                        (
                            item => new Element
                            (
                                new string(item.TakeWhile(symbol => symbol != '(').ToArray())
                            )
                        ).ToHashSet();

                    if (lastReadedChain == null)
                    {
                        lastReadedChain = new Chain(items[0]);
                        chains.Add(lastReadedChain, elements);
                    }
                    else
                    {
                        chains[lastReadedChain].UnionWith(elements);
                    }

                    if (line.EndsWith(";"))
                        lastReadedChain = null;
                }
            );

            return chains;
        }
    }
}