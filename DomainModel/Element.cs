﻿using CodeContracts;

namespace DomainModel
{
    public class Element : Vertex
    {
        public override string Name { get; }

        public Element(string name)
        {
            Requires.NotNull(name, nameof(name), "Имя элемента не может быть равно null");
            Requires.NotNullOrEmpty(name, nameof(name), "Имя элемента не может быть пустым");

            Name = name;
        }
    }
}